(function() {
    var app = angular.module("GrphMeApp", []);
    var earningsData;
    
    app.service("graphService", function($http, $q)
    {
        var deferred = $q.defer();
        $http.get('/data/earnings.json').then(function(data)
        {
            deferred.resolve(data);
        });

        this.getData = function()
        {
            return deferred.promise;
        }
    })
    
    app.controller("YearController", function($scope, graphService)
    {
        var promise = graphService.getData();
        var selectedGraphAddress = "-:-";
        promise.then(function(data) //initialize here
        {
            $(".graphBtn").on("click", function(){var temp = new Object(); temp.keyCode = 27; keyDown(temp);}); //Fake escape
            $scope.earningYears = [];
            $scope.earningYears = data.data;
            var reverseSortedEarnings = [];
            
            //draw year graph intially.
            createGraph(selectedGraphAddress);
        
            refreshGraph();
            
            $(window).resize( resizeGraph );
            $(window).keydown( keyDown );
        });
        
        //Resize the graph my friend
        function resizeGraph()
        {
            clearGraph();
            createGraph(selectedGraphAddress);
            refreshGraph();
        }
        
        //Data path example
        //"-:--"
        //"2013:-", "2014:-", "2015:-"
        //["2013:Q1","2013:Q2","2013:Q3","2013:Q4"] , ["2014:0","2014:1","2014:2","2014:3"]
        function createGraph(dataPath)
        {            
            //Parse dataPath to determine what we're supposed to be graphing.
            //If YEAR in format "YEAR:QUARTER" is equal to "-" graph all years.
                var pathSplit = dataPath.split(":");
                var requestedYear    = pathSplit[0];
                var requestedQuarter = pathSplit[1];
        
                var rawDataSet = $scope.earningYears.children; //Get children here, so we don't have to reference it elsewhere.
                var yearIndex = _.findIndex(rawDataSet, _.matchesProperty('label', parseInt(requestedYear)));
                var quarterIndex;
                try
                {
                    quarterIndex = _.findIndex(rawDataSet[yearIndex].children, _.matchesProperty('label', requestedQuarter));
                }
                catch(err)
                {
                    quarterIndex = -1;
                }
                
            
            //if yearIndex is -1 then we get all the years
                if(yearIndex == -1)
                {
                    var dataSet = rawDataSet;
                }
                else if (quarterIndex == -1) //if quarterIndex is -1 then we get a specific year
                {
                    var dataSet = rawDataSet[yearIndex].children;
                }
                else//if both are populated then we know we are getting the months in a certain quarter in a certain year
                {
                    var dataSet = rawDataSet[yearIndex].children[quarterIndex].children;
                }
            
            //Figure necessary divisions based on what we're graphing.
                var graphWidth = $("#Graph").width();
                var graphHeight = $("#Graph").height(); 
                var xPadding = graphWidth * (1/10);
                var yPadding = graphHeight* (1/10);

                var maximumValue = Number.MIN_VALUE;
                var minimumValue = Number.MAX_VALUE;
            
            //Calculate minimum and maximum values.
                for(var index=0; index < dataSet.length; index++)
                {
                    var tempValue =  dataSet[index].earnings;
                    if(tempValue < minimumValue) minimumValue = tempValue;
                    if(tempValue > maximumValue) maximumValue = tempValue;
                }
            
            //Graph X values
                var xDivisions = dataSet.length;
                var deltaX = graphWidth/xDivisions;
                for(var num = 0; num < xDivisions; num++) //This takes care of drawing our vertical lines.
                {
                    var xPos = (deltaX*num)+(deltaX/2);
                    addGridLine(xPos, yPadding, xPos, graphHeight-yPadding+20);
                    
                    $("#GridLabels").append("<text class=x-labels x='" + (deltaX*num + deltaX/2 - 15) + "' y='" + (graphHeight - yPadding + 40) + "'>" + dataSet[num].label +"</text>");
                    var pointValue = graphHeight - ((dataSet[num].earnings-minimumValue)/(maximumValue-minimumValue) * (graphHeight-yPadding*2)+yPadding);
                    
                    var dataPointPath = "";
                    
                    if(requestedYear == "-")
                        dataPointPath = dataSet[num].label + ":-";
                    else if(requestedQuarter == "-")
                        dataPointPath = requestedYear + ":" + dataSet[num].label;
                    else 
                        dataPointPath = "NULL";
                    
                    // Adding the functionality for path generation
                    if (num == 0) addPathPoint(xPos, graphHeight-yPadding+20);
                    
                    addPathPoint(xPos, pointValue);
                    
                    if (num == xDivisions-1) addPathPoint(xPos, graphHeight-yPadding+20);
                    
                    addDataPoint(xPos, pointValue, dataPointPath, dataSet[num].earnings);
                }
            
            //Graph Y tick marks
                var difference = maximumValue - minimumValue;
                var yDivisions = 5;
                var deltaY = graphHeight/yDivisions;
                var differenceDelta = difference/yDivisions;
                for(var num=0; num < yDivisions-1; num++) //This takes care of drawing our horizontal lines.
                {
                    addGridLine(xPadding, yPadding*2+num*deltaY, graphWidth-xPadding, yPadding*2+num*deltaY);
                    $("#GridLabels").append("<text class=y-labels x='" + xPadding/2 + "' y='" + ( graphHeight - (yPadding*2 + num*deltaY)+6) + "'>" + (differenceDelta<1?(minimumValue+differenceDelta*(num+1)).toFixed(2):Math.trunc(minimumValue+differenceDelta*(num+1))) + "</text>"); 
                } 
                
            //Set graph Title
                $("#graphTitle").text(getGraphFriendlyName());
            //Remove tooltip
                $(".tooltip").remove();
        }
        
        // Adds the actual data points to the graph
        function addDataPoint(x, y, path, toolTipTitle)
        {
            $("#DataPoints").append("<circle cx='" + x + "' cy='" + y + "' r='8' data-path='" + path + "' data-toggle='tooltip' title='$" + toolTipTitle + "'/>");
        }
        
        //Helper function to add Grid Lines.
        function addGridLine(x1, y1, x2, y2)
        {
             $("#Grid").append("<line x1='" + x1 + "' y1='" + y1 + "' x2='" + x2 + "' y2='" + y2 + "'></line>");
        }
        
        //Function to add path line.
        function addPathPoint(x, y)
        {
            var data = $("#graphLines").attr("d");
            data += (data.length==0?"M":"L") + x + "," + y; //Yeah, pretty slick
            $("#graphLines").attr("d", data);
            
            
        }
        
        //Don't forget to refresh after creating the graph.
        function refreshGraph()
        {
            $("#Grid").html( $("#Grid").html() );
            $("#GridLabels").html( $("#GridLabels").html() );
            $("#DataPoints").html( $("#DataPoints").html() );
            $("#Surfaces").html( $("#Surfaces").html() );
            updateGraphListeners();
        }
        
        //So when you resize the graph and switch graphs entirley you need to update their listeners
        function updateGraphListeners()
        {
            $("#DataPoints circle").click(clickDataPoint);
            $("#DataPoints circle").mouseover(mouseOverDataPoint);
            $("#DataPoints circle").mouseout(mouseOutDataPoint);
            $('svg circle').tooltip({
                'container': 'body',
                'placement': 'top'
            });
        }
        
        //Simply just clears the graph
        function clearGraph()
        {
            $("#Grid").html("");
            $("#GridLabels").html("");
            $("#DataPoints").html("");
            $("#graphLines").attr("d", "");
            $("#Datapoints.circle").unbind("click");
        }
        
        //This is the machine behind the graph navigation
        function setSelectedGraph(path)
        {
            if(path=="NULL" || path=="") return;
            
            selectedGraphAddress = path; 
            clearGraph();
            createGraph(selectedGraphAddress);
            $("#graphContainer").removeClass("fadeIn").addClass("fadeIn");
            refreshGraph();
        }
        
        //When clicked send the path to the setSelectedGraph method
        function clickDataPoint(e)
        {
            setSelectedGraph( $(e.target).data("path"));
        }
        
        //Purely aesthetic method to change some of the css properties
        function mouseOverDataPoint(e)
        {
            var jtarget = $(e.target);
            jtarget.css("stroke-width","3");
            jtarget.attr("r","12");
        }
        
        //Purely aesthetic method to change some of the css properties
        function mouseOutDataPoint(e)
        {
            var jtarget = $(e.target);
            jtarget.css("stroke-width","2");
            jtarget.attr("r","8");
        }
        
        //Allows us to use the escape key to navigate back up the graph tree
        function keyDown(e)
        {
            if(e.keyCode == 27) //ESCAPE
            {
                var splitResult = selectedGraphAddress.split(":");
                if(splitResult[1] != "-")
                {
                    setSelectedGraph(splitResult[0] + ":-");
                }
                else if(splitResult[0] != "-")
                {
                    setSelectedGraph("-:-");
                }
            }
        }
        
        function getGraphFriendlyName()
        {
            var split = selectedGraphAddress.replace("-", " ").replace("-", " ").split(":"); //Yes we are cheating, sorry.
            var str = split[0] + " " + split[1]; 
            return (str.length==3?"All Years":str); //If str is "empty" there will be three spaces. Not exactly robust. Three is the minimum it can have. 
        }
    })
})();