# README #

* Running the site: 

     Open Index.html through brackets or any server software.

* Quick summary:

    With GrphMe, you can graph company earnings by year, quarter, and month.
    To navigate:
        Click on data points.
        Use escape key or up arrow button to zoom out or in. 
    
* Process:

    Get's graph data through angularJS http service from JSON file.
    Uses ng-controller and ng-repeat to generate data table.
    Uses SVG elements to draw the graph from the controller. 
    